<?php

if (!defined('MEDIAWIKI')) {
        echo <<<EOT
To install my extension, put the following line in LocalSettings.php:
require_once( "\$IP/extensions/MyExtension/MyExtension.php" );
EOT;
        exit( 1 );
}

$wgExtensionCredits['validextensionclass'][] = array(
   'name' => 'Voter',
   'author' =>'iris72', 
   'description' => 'Vote extension for mediawiki',
   'version' => '0.0.0'
   );
   
   
$dir = dirname(__FILE__) . '/';
   
# Schema updates for update.php
$wgHooks['LoadExtensionSchemaUpdates'][] = 'fnMyHook';
function fnMyHook( DatabaseUpdater $updater ) {
        $updater->addExtensionUpdate( array( 'addTable', 'voter_vote',
                dirname( __FILE__ ) . '/schema/db.sql', true ) );
        return true;
}

$wgAutoloadClasses['VoterActionsPage'] = $dir . 'VoterActionsPage.php';
$wgSpecialPages['VoterActionsPage'] = 'VoterActionsPage';
$wgExtensionMessagesFiles['Voter'] = $dir . 'voter.i18n.php';
