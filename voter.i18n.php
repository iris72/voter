<?php
$messages=array();
$messages['ru'] = array(
    "voter-not-logined" => "Вы не являетесь зарегистрированным пользователем",
    "voter-action-name-is-empty" => "Укажите название мероприятия",
    "voter-action-exists" => "Мероприятие с таким именем уже есть",
);
