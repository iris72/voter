
CREATE TABLE /*_*/voter_actions (
action_id int(10) unsigned not null PRIMARY KEY AUTO_INCREMENT,
user_id int(10) unsigned not null, # admin reference
action_name varchar(255) not null,
action_descr varchar(255),
UNIQUE (action_name)
) /*$wgDBTableOptions*/;

CREATE TABLE /*_*/voter_section (
section_id int(10) unsigned not null PRIMARY KEY AUTO_INCREMENT,
section_token char(16) not null,
action_id int(10) unsigned not null, # reference to action
section_name varchar(255) not null,
section_descr varchar(255),
UNIQUE (section_token),
UNIQUE (action_id, section_name)
) /*$wgDBTableOptions*/;

CREATE TABLE /*_*/voter_anonymus (
anonymus_id int(10) unsigned PRIMARY KEY AUTO_INCREMENT,
section_id int(10) unsigned not null, # reference to section
anonymus_token char(16) not null,
UNIQUE (section_id, anonymus_token)
) /*$wgDBTableOptions*/;

CREATE TABLE /*_*/voter_question (
question_id int(10) unsigned not null PRIMARY KEY AUTO_INCREMENT,
section_id int(10) unsigned not null, # reference to section
question_type char(20) not null,
question_is_active bool not null default FALSE,
question_text varchar(255) not null,
question_descr varchar(255)
) /*$wgDBTableOptions*/;

CREATE TABLE /*_*/voter_question_answer (
question_answer_id int(10) unsigned not null PRIMARY KEY AUTO_INCREMENT,
question_id int(10) unsigned not null, # reference to question
question_answer_text varchar(255) not null,
UNIQUE (question_answer_text, question_id)
) /*$wgDBTableOptions*/;

CREATE TABLE /*_*/voter_vote (
voter_id int(10) unsigned not null PRIMARY KEY AUTO_INCREMENT,
question_answer_id int(10) unsigned not null, # reference to question answer
anonymus_id int(10) unsigned not null, # reference to anonymous
voter_rating int not null
) /*$wgDBTableOptions*/;


ALTER TABLE /*_*/voter_actions ADD CONSTRAINT FOREIGN KEY (user_id) REFERENCES user (user_id);
ALTER TABLE /*_*/voter_anonymus ADD CONSTRAINT FOREIGN KEY (section_id) REFERENCES /*_*/voter_section(section_id);
ALTER TABLE /*_*/voter_question ADD CONSTRAINT FOREIGN KEY (section_id) REFERENCES /*_*/voter_section(section_id);
ALTER TABLE /*_*/voter_question_answer ADD CONSTRAINT FOREIGN KEY (question_id) REFERENCES /*_*/voter_question(question_id);
ALTER TABLE /*_*/voter_vote ADD CONSTRAINT FOREIGN KEY (question_answer_id) REFERENCES /*_*/voter_question_answer(question_answer_id);
ALTER TABLE /*_*/voter_vote ADD CONSTRAINT FOREIGN KEY (anonymus_id) REFERENCES /*_*/voter_anonymus(anonymus_id);
