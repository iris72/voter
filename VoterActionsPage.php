<?php



class VoterActionsPage extends SpecialPage {
        function __construct() {
                parent::__construct( 'VoterActionsPage' );
                wfLoadExtensionMessages( 'VoterActionsPage' );
        }
        
        private function makeAddActionForm() {
            ob_start();
            global $wgServer;
            ?>
            <form action="<?php echo $wgServer ?>/Special:VoterActionsPage" method="post">
                <input type="hidden" name="action" value="create_action" />
                <input type="text"name="action_name" />
                <input type="submit" value="Добавить" />
            </form>
            <?php
            return ob_get_clean();
        }
        
        private function wgWkiOut($par) {
            global $wgOut;
            $wgOut->addWikiText('===' . wfMsg($par) . '===');
        }
        
        private function createAction($dbr) {
            global $wgRequest, $wgUser;
            $aname = $wgRequest->getText("action_name");
            if (!$wgUser->isLoggedIn()) {
                $this->wgWkiOut('voter-not-logined');
                return false;
            }
            if (empty($aname)) {
                $wgOut->addWikiText('===' . wfMsg('voter-action-name-is-empty') . '===');
                $this->wgWikiOut('voter-action-name-is-empty');
                return false;
            }
            $r = $dbr->selectRow('voter_actions', 'action_id', "action_name = '$aname'", __METHOD__);
            if ($r != false) {
                $this->wgWkiOut('voter-action-exists');
                return false;
            }
            $dbr->insert('voter_actions', array(
                'user_id' => $wgUser->getID(),
                'action_name' => $aname,
            ), __METHOD__);
            
            
            return true;
        }
        
        private function makeMyActionsList($dbr) {
            ob_start();
            global $wgUser;
            ?>
            <?php if ($wgUser->isLoggedIn()): ?>
                <ul>
                    
                </ul>
            <?php endif ?>
            <?php
            return ob_get_clean();
        }
        
        private function makeNotMyActionList($dbr) {
            return '';
        }
        
        function execute( $par ) {
            global $wgRequest, $wgOut, $wgUser;
            $this->setHeaders();
            
            $dbr = wfGetDB(DB_MASTER);
            $dbr->begin();
            
            $action=$wgRequest->getText("action");
            switch($action) {
                case "create_action":
                    if (!$this->createAction($dbr)) return true;
                break;
            }
            
            if ($wgUser->isLoggedIn()) {
                $wgOut->addHTML($this->makeAddActionForm());
                $wgOut->addHTML($this->makeMyActionsList($dbr));
            }
            $wgOut->addHTML($this->makeNotMyActionList($dbr));
           
            $dbr->commit();
        }
}